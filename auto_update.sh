#!/bin/bash

# Originally made by Wiethoofd
# Modified and converted into Bash by JarateKing
# Modified by sealeaf to use vpk

[ -f /usr/local/bin/vpk ] || {
    echo "installing vpk"
    printf '#!/bin/bash \nVPK_LINUX=$(find "${HOME}/.local/share/Steam" -type f -iname "vpk_linux32" -print | head -n 1) \nVALVE_LIB_DIR=$(dirname "${VPK_LINUX}") \nLD_LIBRARY_PATH="${VALVE_LIB_DIR}:${LD_LIBRARY_PATH}" "${VPK_LINUX}" "${@}" \n' |\
        sudo tee /usr/local/bin/vpk
    sudo chmod +x /usr/local/bin/vpk
}

# Use vpk to get default HUD files ( https://developer.valvesoftware.com/wiki/VPK#Linux_.2F_Unix )
# scheme files
vpk -p "../../tf2_misc_dir.vpk" -d "resource/base" -e "root\resource\chatscheme.res" -m -v -s
vpk -p "../../tf2_misc_dir.vpk" -d "resource/base" -e "root\resource\clientscheme.res" -m -v -s
vpk -p "../../tf2_misc_dir.vpk" -d "resource/base" -e "root\resource\gamemenu.res" -m -v -s
vpk -p "../../tf2_misc_dir.vpk" -d "resource/base" -e "root\resource\sourcescheme.res" -m -v -s
# script files
vpk -p "../../tf2_misc_dir.vpk" -d "scripts/base" -e "root\scripts\hudlayout.res" -m -v -s
vpk -p "../../tf2_misc_dir.vpk" -d "scripts/base" -e "root\scripts\hudanimations_tf.txt" -m -v -s
vpk -p "../../tf2_misc_dir.vpk" -d "scripts/base" -e "root\scripts\mod_textures.txt" -m -v -s
# resource files
vpk -p "../../tf2_misc_dir.vpk" -d "resource/ui/base" -e "root\resource\ui" -m -v -s
mv resource/ui/base/ui/* resource/ui/base/
rm -rf resource/ui/base/ui/

# replace minmode lines
sed -i 's/minmode/minbad/gI' scripts/base/hudlayout.res
sed -i 's/minmode/minbad/gI' resource/ui/base/*

# create default animations
sed -i 's/event /event DefaultAnim/g' scripts/base/hudanimations_tf.txt
