#!/usr/bin/env bash

## ASI escape codes for colors
# Black        0;30     Dark Gray     1;30
# Red          0;31     Light Red     1;31
# Green        0;32     Light Green   1;32
# Brown/Orange 0;33     Yellow        1;33
# Blue         0;34     Light Blue    1;34
# Purple       0;35     Light Purple  1;35
# Cyan         0;36     Light Cyan    1;36
# Light Gray   0;37     White         1;37

## color vars
BLACK='\033[0;30m'
RED='\033[0;31m'
GREEN='\033[0;32m'
ORANGE='\033[0;32m'
BLUE='\033[0;34m'
PURPLE='\033[0;35m'
CYAN='\033[0;36m'
GRAY='\033[0;37m'
NC='\033[0m' # No Color

## More vars
TFDEF="$HOME/.steam/debian-installation/steamapps/common/Team\ Fortress\ 2/" # default install dir

# Header
echo "UberHud linux installer"

## Download
echo -e "${CYAN}Making temporary directories${NC}"
mkdir /tmp/uberhud
echo -e "${CYAN}Downloading UberHud ${NC}"
curl https://codeberg.org/raspimana/UberHud/archive/master.tar.gz --output \
    /tmp/uberhud/uberhud.tar.gz || \
    echo -e "${READ}ERROR: Download Failed${RED}"

## Install

# if user types "Y" or "y" are used the defualt path
# if user types anything else it will use it as a path

echo "${CYAN} Where Team Fortress 2 installed in [Y/y for Default or Path for custom] ${NC}:"
echo "$TFDEF [Default]"
read check
if [[ $check =~ ^[Yy]$ ]]
then
    [ -d "$TFDEF" ] || {
        echo -e "${READ}ERROR${NC} $TFDEF does not exist!"
    }
    echo "Installing UberHud into $TFDEF/tf/custom"
    echo -e "${GREEN}Making 'custom' directories${NC}"
    mkdir $HOME/.steam/debian-installation/steamapps/common/Team\ Fortress\ 2/tf/custom
    #mkdir $HOME/.steam/debian-installation/steamapps/common/Team\ Fortress\ 2/tf/custom/uberhud
    echo -e "${GREEN} Extracting UberHud! ${NC}"
    tar -xvf /tmp/uberhud/uberhud.tar.gz -C \
        $HOME/.steam/debian-installation/steamapps/common/Team\ Fortress\ 2/tf/custom/ || \
        echo -e "${RED}ERROR${NC}: extract failed!"
    echo -e "${GREEN}UberHud is Now Installed!${NC}"
else
    [ -d $check ] || {
        echo -e "${READ} Team Fortress 2 is Not Installed into $check2 ${NC}"
    }
    TFREAL=$check
    echo -e "${GREEN}Making 'custom' directorys${NC}"
    mkdir $TFREAL/tf/custom
    #mkdir $TFREAl/tf/custom/uberhud
    printf "Installing UberHud into \n $TFREAL/tf/custom \n"
    mkdir $TFREAL/tf/custom
    tar -xvf /tmp/uberhud/uberhud.tar.gz -C \
        $TFREAL/tf/custom/ || \
        echo -e "${RED}ERROR${NC}: extract failed!"
        echo -e "${GREEN}UberHud is Now Installed!${NC}"
#            echo -e "${READ}ERROR${NC}: Somthing went wrong!"
fi
