#!/usr/bin/sh

## always add / create class cfg's in cfg
cat ./dev/ref/add_to_class.cfg | tee -a  ../..{cfg,cfg/user}/{scout,soldier,pyro,demoman,heavyweapons,engineer,medic,sniper,spy}.cfg

## check all custom folders as well
cat ./dev/ref/add_to_class.cfg | tee -a  ../*/{cfg,cfg/user}/{scout,soldier,pyro,demoman,heavyweapons,engineer,medic,sniper,spy}.cfg
