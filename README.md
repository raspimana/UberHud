# UberHud

Another TF2 hud using [JarateKing's base](https://github.com/JarateKing/BaseHud)
## Heads up!
This is a work in progress and some features may be unfinished, or missing. This project can also cease development at any time.

## Setting Up
1. Download the zip file [here.](https://codeberg.org/raspimana/UberHud/archive/master.zip)
2. WINDOWS: Copy the folder to `[Steam Instalation Folder]\steamapps\common\team fortress 2\tf\custom\UberHud`. Make the custom folder if you don't have it.
2. LINUX:
    - Dependencies: `curl` `tar` `bash`
    - Run to download and install`curl -sSL https://codeberg.org/raspimana/UberHud/raw/branch/master/linux_install.sh | bash`
    - Note: `sudo` is also required to run `auto_update.sh`
3. Run setup_cfg. Use the one ending in `.bat` for windows, and `.sh` for linux.
4. Enjoy!

## Direct contact

If you need any help, you can contact me on Telegram: @raspwashere

## Captions
For a quick reminder, you can type "hud_captions_help" in console to get the description. If you don't know how to enable the console, [then here's a good guide. ](https://steamcommunity.com/sharedfiles/filedetails/?id=217543736)You need to do that because it is too long to fit here. But the bases are voicelines, miscellaneous effects, scripts that use cc_emit, stock tf2, the de facto standard, the hud's default, and disable captions.

## Basic streamer mode
To disable text/voice chat, execute "hud_streamer" in console (exec hud_streamer). Same thing, if you don't know how to enable the console, [then here's a good guide. ](https://steamcommunity.com/sharedfiles/filedetails/?id=217543736)To disable sprays, execute "hud_disable_spray" (exec hud_disable_spray). ⚠ NOTE THAT THIS WILL KICK YOU FROM THE GAME! ⚠
## Full streamer mode.
The basic streamer mode will just disable chat/sprays. If you want a full streamer hud, then move streamermode.res from /customizations to the "movehere" folder in it.
<!-- TODO: streamermode.res is a stub right now, fix it! -->